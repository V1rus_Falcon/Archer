<?php
class ArcherCurl {
    public $cookie_file;
    public $follow_redirects = true;
    public $headers = [];
    public $options = [];
    public $referer;
    public $user_agent;
    protected $error = '';
    protected $request;
    function __construct() {
        $this->cookie_file = dirname(__FILE__).DIRECTORY_SEPARATOR.'curl_cookie.txt';
        $this->user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'Acher FrameWork';
    }
    function delete($url, $vars = array()) {
        return $this->request('DELETE', $url, $vars);
    }
    function error() {
        return $this->error;
    }
    function get($url, $vars = array()) {
        if (!empty($vars)) {
            $url .= (stripos($url, '?') !== false) ? '&' : '?';
            $url .= (is_string($vars)) ? $vars : http_build_query($vars, '', '&');
        }
        return $this->request('GET', $url);
    }
    function head($url, $vars = array()) {
        return $this->request('HEAD', $url, $vars);
    }
    function post($url, $vars = array()) {
        return $this->request('POST', $url, $vars);
    }
    function put($url, $vars = array()) {
        return $this->request('PUT', $url, $vars);
    }
    function request($method, $url, $vars = array()) {
        $this->error = '';
        $this->request = curl_init();
        if (is_array($vars)) $vars = http_build_query($vars, '', '&');
        $this->set_request_method($method);
        $this->set_request_options($url, $vars);
        $this->set_request_headers();
        $response = curl_exec($this->request);
        $this->error = curl_errno($this->request).' - '.curl_error($this->request);
        $this->error = curl_getinfo($this->request, CURLINFO_RESPONSE_CODE);
        curl_close($this->request);
        return $response;
    }
    protected function set_request_headers() {
        $headers = array();
        foreach ($this->headers as $key => $value) {
            $headers[] = $key.': '.$value;
        }
        curl_setopt($this->request, CURLOPT_HTTPHEADER, $headers);
    }
    protected function set_request_method($method) {
        switch (strtoupper($method)) {
            case 'HEAD':
                curl_setopt($this->request, CURLOPT_NOBODY, true);
                break;
            case 'GET':
                curl_setopt($this->request, CURLOPT_HTTPGET, true);
                break;
            case 'POST':
                curl_setopt($this->request, CURLOPT_POST, true);
                break;
            default:
                curl_setopt($this->request, CURLOPT_CUSTOMREQUEST, $method);
        }
    }

    protected function set_request_options($url, $vars) {
        curl_setopt($this->request, CURLOPT_URL, $url);
        if (!empty($vars)) curl_setopt($this->request, CURLOPT_POSTFIELDS, $vars);
        curl_setopt($this->request, CURLOPT_HEADER, false);
        curl_setopt($this->request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->request, CURLOPT_USERAGENT, $this->user_agent);
        if ($this->cookie_file) {
            curl_setopt($this->request, CURLOPT_COOKIEFILE, $this->cookie_file);
            curl_setopt($this->request, CURLOPT_COOKIEJAR, $this->cookie_file);
        }
        if ($this->follow_redirects) curl_setopt($this->request, CURLOPT_FOLLOWLOCATION, true);
        if ($this->referer) curl_setopt($this->request, CURLOPT_REFERER, $this->referer);
        foreach ($this->options as $option => $value) {
            curl_setopt($this->request, constant('CURLOPT_'.str_replace('CURLOPT_', '', strtoupper($option))), $value);
        }
    }
}


class ArcherException extends Exception{}

class ArcherWebHook {
	var $callback_query;
	private $edited_message;
  var $inline_query;


 	var $chat;
 	var $message;
 	var $from;
  var $id;
  var $data;

	public function __construct(bool $bool) {
		if($bool) $this->jsonManager(json_decode(file_get_contents('php://input'), true));
		return $this;
	}

	private function jsonManager(array $array) {
		if(isset($array['message'])){
      $this->message = $array['message'];
      $this->chat = $this->message['chat'];
			$this->chat['type'] = 'message';

      $this->message = $this->message;
			$this->message['type'] = 'message';

      $this->from = $this->message['from'];
      $this->from['type'] = 'message';
    }
		if(isset($array['callback_query'])){
      $this->callback_query = $array['callback_query'];
      $this->message = $array['callback_query']['message'];
      $this->chat = $this->callback_query['message']['chat'];
			$this->chat['type'] = 'callback_query';

      $this->message = $this->callback_query['message'];
      $this->message['type'] = 'callback_query';

      $this->from = $this->callback_query['from'];
      $this->from['type'] = 'callback_query';
    }
		if(isset($array['edited_message'])){
      $this->message = $array['edited_message'];
      $this->edited_message = $this->message;
      $this->chat = $this->edited_message['chat'];
			$this->chat['type'] = 'edited_message';

      $this->message = $this->edited_message;
      $this->message['type'] = 'edited_message';

      $this->from = $this->edited_message['from'];
      $this->from['type'] = 'edited_message';
    }
    if(isset($array['inline_query'])) {
      $this->inline_query = $array['inline_query'];
      $this->from = $array['inline_query']['from'];
      $this->from['type'] = "inline_query";
      $this->id = $this->inline_query['id'];
    }
    if(isset($array['chosen_inline_result'])){
      $this->chosen_inline_result = $array['chosen_inline_result'];
      $this->from = $this->chosen_inline_result['from'];
      $this->from['type'] = "chosen_inline_result";
    }
	}
}

class Archer {
	private $token;
	private $BASE_URL = "https://api.telegram.org/bot";
	private $BOT_URL;
	private $resend;
	private $settings;
	private $sp;
	private $fullResponse;
  private $keyboard;

  var $reply_to_message_id;
  var $rtmi;
	var $isOk;
	var $response;

	public function setSettings(array $arg) {
		if(!is_array($arg)) throw new ArcherException('Settings value must be an array');
		if(isset($arg['settings'])) $this->settings = $arg['settings'];
		$this->token = $arg['token'];
		$this->BOT_URL = $this->BASE_URL . $this->token;
		return $this;
	}

	public function __construct(array $arg = null) {
		if (isset($arg)) $this->setSettings($arg);
	}

	private function jsonManager() {
		if(!isset($this->settings)) throw new ArcherException('Settings cannot be unseted');
		$r = json_decode($this->fullResponse, true);
		if($r['ok'] == 0) $this->isOk = 0;
		else $this->isOk = 1;
		if($this->isOk) $this->response = $r['result'];
	}

  public function setKeyboard ($array) {
    if(is_array($array)) $this->keyboard = $array;
    elseif(json_decode($array, true) != null) $this->keyboard = json_decode($array, true);
    else throw new ArcherException('Keyboard has got error');
    return $this;
  }

  public function kb($array) {
    $this->setKeyboard($array);
  }

	public function sendMessage (int $chat_id, $text) {
		if(!isset($chat_id)) throw new ArcherException('Chat id cannot be null');
		if(!isset($text)) throw new ArcherException('Text cannot be null');
		$array = [
			'chat_id' => $chat_id,
			'text' => $text
		];
		$this->sp = [$this->BOT_URL."/sendMessage", $array];
		return $this;
	}

	public function editMessage(int $chat_id, int $msg_id, $text){
		$array = [
			'chat_id' => $chat_id,
			'text' => $text,
			'message_id' => $msg_id
		];
		$this->sp = [$this->BOT_URL."/editMessageText", $array];
		return $this;
  }
  
  public function editKeyboard($chat_id, $message_id) {
    $array = [
      'chat_id' => $chat_id,
      'message_id' => $message_id
    ];
    $this->sp = [$this->BOT_URL."/editMessageReplyMarkup", $array];
    return $this;
  }

  public function sendPhoto(int $chat_id, $photo, $caption) {
    $array = [
      'chat_id' => $chat_id,
      'photo' => $photo,
      'caption' => $caption
    ];
    $this->sp = [$this->BOT_URL."/sendPhoto", $array];
    return $this;
  }

  public function deleteMessage(int $chat_id, int $msg_id) {
    $array = [
      'chat_id' => $chat_id,
      'message_id' => $msg_id
    ];
    $this->sp = [$this->BOT_URL."/deleteMessage", $array];
    return $this;
  }

  public function forwardMessage(int $from_id, int $to_id, int $msg_id) {
    $array = [
      'chat_id' => $to_id,
      'from_chat_id' => $from_id,
      'message_id' => $msg_id
    ];
    $this->sp = [$this->BOT_URL."/forwardMessage", $array];
    return $this;
  }

  public function answerCallbackQuery(string $callback_query_id, string $text, int $show_alert = 1){
    $array = [
      'callback_query_id' => $callback_query_id,
      'text' => $text,
      'show_alert' => $show_alert
    ];
    $this->sp = [$this->BOT_URL."/answerCallbackQuery", $array];
    return $this;
  }

  public function answerInlineQuery(string $inline_query_id, array $results, int $cache_time = 1) {
    $array = [
      'inline_query_id' => $inline_query_id,
      'results' => json_encode($results),
      'cache_time' => $cache_time
    ];
    $this->sp = [$this->BOT_URL."/answerInlineQuery", $array];
    return $this;
  }

  public function getWebhookInfo() {
    $this->sp = [$this->BOT_URL."/getWebhookInfo"];
    return $this;
  }
	public function send() {
		$c = new ArcherCurl;
		if(isset($this->sp[1])) $ar = $this->sp[1];
    else $ar = [];
		if (isset($this->settings['parse_mode'])) $ar['parse_mode'] = $this->settings['parse_mode'];
    else $ar['parse_mode'] = 'HTML';

    if (isset($this->settings['disable_notification']))  $ar['disable_notification'] = $this->settings['disable_notification'];
    else $ar['disable_notification'] = 0;

    if (isset($this->settings['disable_web_page_preview'])) $ar['disable_web_page_preview'] = $this->settings['disable_web_page_preview'];
    else $ar['disable_web_page_preview'] = 0;

    if(isset($this->keyboard)) $ar['reply_markup'] = json_encode(['inline_keyboard' => $this->keyboard]);
    if(isset($this->reply_to_message_id)) $ar['reply_to_message_id'] = $this->reply_to_message_id;
    if(isset($this->rtmi)) $ar['reply_to_message_id'] = $this->rtmi;
		$this->fullResponse = $c->post($this->sp[0], $ar);
		$this->jsonManager();
    if (isset($this->keyboard)) unset($this->keyboard);
    if (isset($this->reply_to_message_id)) unset($this->reply_to_message_id);
    if (isset($this->rtmi)) unset($this->rtmi);
    unset($c);
		return $this;
  }
  public function __toString() {
    return json_encode($this->response);
  }
  public function afterAction($d) {
    $this->afsp = $d->sp;
    return $this->afsp;
  }
}
class ArcherKeyboard {
	public $lines = [];

  var $ae;
  var $exist;
  public function __construct(){}
	public function rowKeyBoard (array $arg) {
		if(!is_array($arg)) throw new ArcherException('RowKeyBoard must be an array');
		$this->lines = $arg;
		return $this;
	}
  public function alredy_exist(int $line, int $column){
    if(isset($this->lines[$line][$column]))
      $this->ae = $this->lines[$lines][$columns];
      $this->exist = true;
    return $this;
  }
	public function guidedUrlKeyBoard(int $line, int $column, string $text, string $url) {
		$this->lines[$line][$column] = ['text' => $text, 'url' => $url];
		return $this;
	}
	public function guidedCallBackKeyBoard(int $line, int $column, string $text, string $data) {
		$this->lines[$line][$column] = ['text' => $text, 'callback_data' => $data];
		return $this;
  }
  public function guidedSwitchCKeyBoard($line, $column, $title, $value) {
    $this->lines[$line][$column] = ['text' => $title, 'switch_inline_query_current_chat' => $value];
    return $this;
  }
  public function export() {
    return $this->lines;
  }
  public function __toString() {
    return json_encode($this->lines);
  }
}
class ArcherInline{
  public $array;
  private $settings = ['parse_mode' => 'HTML'];
  public function inlineQuery(string $type, string $id, string $title, string $message_text) {
    $this->array[] = [
      'type' => $type,
      'id' => $id,
      'message_text' => $message_text,
      'title' => $title,
      'parse_mode' => $this->settings['parse_mode']
    ];
    return $this;
  }
}
class ArcherStock {
  public function save($name, $value) {
    $this->$name = $value;
    return $this;
  }
}
class ArcherExtra {
  public function __construct() {
    $ver = "Archer alpha 0.4 mrk.1";
    return $ver;
  }
  public function update(){
    rename('Archer.php', 'Archer.del');
    copy('https://archer.mushidesign.com/Archer.v1f', 'Archer.php');
    unlink('Archer.del');
  }
}
?>
